<?php

namespace Uccello\Uccello\Http\Controllers;

use Illuminate\Http\Request;
use Spatie\Menu\Laravel\Facades\Menu;
use Spatie\Menu\Html;

class ListController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function process($domain, $module)
    {
        Menu::macro('main', function () {
            return Menu::new()
                ->addClass('sidebar-menu')
                ->add(Html::raw('<li class="header">MAIN NAVIGATOR</li>'))
                ->action('HomeController@index', Html::raw('<i class="fa fa-home"></i><span>Home</span>')->render())
                // ->action('AboutController@index', 'About')
                // ->action('ContactController@index', 'Contact')
                ->setActiveFromRequest();
        });

        
        return view('uccello::list.main');
    }
}
